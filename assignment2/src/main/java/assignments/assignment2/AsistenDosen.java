package assignments.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class AsistenDosen {
    private List<Mahasiswa> mahasiswas = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        // TODO: buat constructor untuk AsistenDosen.
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        // TODO: kembalikan kode AsistenDosen.
        return kode;
    }

    public String getNama() {
        return nama;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        // TODO: tambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan.
        // Hint: kamu boleh menggunakan Collections.sort atau melakukan sorting manual.
        // Note: manfaatkan method compareTo pada Mahasiswa.
        mahasiswas.add(mahasiswa);
        Collections.sort(mahasiswas);
        for (Mahasiswa m:mahasiswas) {
            mahasiswa.compareTo(m);
        }
    }

    public Mahasiswa getMahasiswa(String npm) {
        // TODO: kembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.

            for (Mahasiswa m: mahasiswas) {
                if(m.getNpm().equals(npm)){
                    return m;
                }
            }


        return null;
    }

    public String rekap() {
        String result = "";
        for (Mahasiswa m: mahasiswas
             ) {
            result = m.toString()+"\n"+m.rekap();
        }
        return result;
    }

    public String toString() {
        return getKode()+" - "+getNama();
    }
}
