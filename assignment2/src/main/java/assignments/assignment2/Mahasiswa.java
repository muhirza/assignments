package assignments.assignment2;

import java.lang.reflect.Array;


public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;


    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // TODO: buat constructor untuk Mahasiswa.
        // Note: komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
    	this.npm = npm;
    	this.nama = nama;
    	this.komponenPenilaian = komponenPenilaian;
    	
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // TODO: kembalikan KomponenPenilaian yang bernama namaKomponen.
        // Note: jika tidak ada, kembalikan null atau lempar sebuah Exception.

            for (KomponenPenilaian kom:komponenPenilaian) {
                if(namaKomponen.equals(kom.getNama())){
                    return kom;
                }
            }



        return null;
    }

    public String getNpm() {
        // TODO: kembalikan NPM mahasiswa.
        return npm;
    }

    public String getNama() {
        return nama;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilai Akhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public String rekap() {
        // TODO: kembalikan rekapan sesuai dengan permintaan soal.
        String nilaiAkhir = "Nilai akhir: ";
        String rerata = "Rerata ";
        String huruf = "Huruf: ";
        String lulus = "";
        double nilaiAkhirs = 0;
        for (KomponenPenilaian kom:komponenPenilaian
             ) {
            rerata = rerata +kom.getNama()+": "+kom.getRerata()+"\n";
            nilaiAkhirs = kom.getNilai();
        }
        nilaiAkhir = nilaiAkhir+nilaiAkhirs+"\n";
        huruf = huruf+getHuruf(nilaiAkhirs)+"\n";
        lulus = getKelulusan(nilaiAkhirs);
        return rerata+nilaiAkhir+huruf+lulus;

    }

    public String toString() {
        // TODO: kembalikan representasi String dari Mahasiswa sesuai permintaan soal.
        return getNpm()+" - "+getNama();
    }

    public String getDetail() {
        // TODO: kembalikan detail dari Mahasiswa sesuai permintaan soal.
        String result = "";
        String nilaiAkhir = "Nilai akhir: ";
        String huruf = "Huruf: ";
        String lulus = "";
        double nilaiAkhirs = 0;
        if(null != komponenPenilaian){
            for (KomponenPenilaian kom:komponenPenilaian
                 ) {
                    result = result+kom.getDetail()+"\n";
                    nilaiAkhirs = kom.getNilai();
            }
            nilaiAkhir = nilaiAkhir+nilaiAkhirs+"\n";
            huruf = huruf+getHuruf(nilaiAkhirs)+"\n";
            lulus = getKelulusan(nilaiAkhirs);
        }

        return result+nilaiAkhir+huruf+lulus;
    }

    @Override
    public int compareTo(Mahasiswa other) {
        // TODO: definisikan cara membandingkan seorang mahasiswa dengan mahasiswa lainnya.
        // Hint: bandingkan NPM-nya, String juga punya method compareTo.

        return getNpm().compareTo(other.getNpm());
    }

}


