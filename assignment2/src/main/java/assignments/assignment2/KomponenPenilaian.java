package assignments.assignment2;


import java.util.ArrayList;

public class KomponenPenilaian {
    private String nama;
    private int bobot;
    private ButirPenilaian[] butirPenilaian;
    public int banyakButirPenilaian = (butirPenilaian == null?0:butirPenilaian.length);
    
    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        // TODO: buat constructor untuk KomponenPenilaian.
        // Note: banyakButirPenilaian digunakan untuk menentukan panjang butirPenilaian saja
        // (tanpa membuat objek-objek ButirPenilaian-nya).
        this.nama = nama;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
        this.bobot = bobot;
    	
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    private KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        // TODO: masukkan butir ke butirPenilaian pada index ke-idx.

            if(null != butir){
                butirPenilaian[idx] = butir;
            }

    }

    public String getNama() {
        // TODO: kembalikan nama KomponenPenilaian.
        return nama;
    }

    public double getRerata() {
        // TODO: kembalikan rata-rata butirPenilaian.
    	 int counta = 0;
         double nilaiButir = 0.0;
         for (int i = 0; i < butirPenilaian.length; i++){
             if(butirPenilaian[i] != null){
                 counta++;
                 nilaiButir += butirPenilaian[i].getNilai();
             }
             else{
                 continue;
             }
         }
         return nilaiButir/counta;
    }

    public double getNilai() {
        // TODO: kembalikan rerata yang sudah dikalikan dengan bobot.
        double rata2 = this.getRerata();

        return rata2 * this.bobot / 100;
    }

    public String getDetail() {
        // TODO: kembalikan detail KomponenPenilaian sesuai permintaan soal.
    String result = "~~~ "+getNama()+" ("+bobot+"%)"+" ~~~\n";
    String resultDetail = "";
    String reRata = "Rerata: "+getRerata()+"\n";
    String nilaiAkhir = "Kontribusi nilai akhir: "+getNilai();
    int z = 1;
        if(null != butirPenilaian){
            for (ButirPenilaian b:butirPenilaian
                 ) {
                z++;
                if(banyakButirPenilaian > 1) {
                    resultDetail = resultDetail+getNama() + " " + z + ": " + b.toString() + "\n";
                }else if(banyakButirPenilaian == 1){

                    resultDetail = resultDetail+getNama() + " " + z + ": " + b.toString() + "\n";
                }
            }
        }
        return result+resultDetail+reRata+nilaiAkhir;
    }

    @Override
    public String toString() {
        // TODO: kembalikan representasi String sebuah KomponenPenilaian sesuai permintaan soal.
        String result = "";
        int z = 1;
        if(banyakButirPenilaian > 1) {
            for(int i=0; i < banyakButirPenilaian;i++){
                z++;
                result = "Rerata "+getNama()+" "+z+": "+getRerata();
                System.out.println(result);
            }
        }else if(banyakButirPenilaian == 1){
            result = "Rerata "+getNama()+": "+getRerata();
            System.out.println(result);
        }
        System.out.println(result);
        return result;
    }

}
