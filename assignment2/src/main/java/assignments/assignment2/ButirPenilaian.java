package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;

    public ButirPenilaian(double nilai, boolean terlambat) {
        // TODO: buat constructor untuk ButirPenilaian.
        this.nilai=nilai;
        this.terlambat=terlambat;
    }

    public double getNilai() {
        // TODO: kembalikan nilai yang sudah disesuaikan dengan keterlambatan.
        double result = 0;
        if(this.terlambat){
            result = nilai - PENALTI_KETERLAMBATAN;
        }else{
            result = nilai;
        }
        return result;
    }

    @Override
    public String toString() {
        // TODO: kembalikan representasi String dari ButirPenilaian sesuai permintaan soal.
        String result = "";
        if(this.terlambat){
            result = String.valueOf(this.getNilai()+" (T)");
        }else{
            result = String.valueOf(this.getNilai());
        }
        return result;
    }
}
