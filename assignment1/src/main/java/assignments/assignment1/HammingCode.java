package assignments.assignment1;

import java.util.Scanner;
import java.util.Arrays;
public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    public static int[] convert_string_intarray (String s){
        String ss=s;
        String[] integerStrings = ss.split(","); 
        int[] integers = new int[integerStrings.length]; 
        for (int i = 0; i < integers.length; i++){
            integers[i] = Integer.parseInt(integerStrings[i]); 
    
        }
        return integers;
    }
    
    public static int[] letakan_redundan(int[] arr,int ukuran,int ukuran_baru,int pangkat){     
        int ar_baru[]= new int[ukuran_baru];
        int ar_lama[]= new int[ukuran];
        ar_lama=arr;
        int j= 0;
        
        for (int i=0;i<pangkat;i++){// memberi nilai 9 pada nilai yang kosong (sebagai penanda)
            /*if (i==0){
                ar_baru[0]=9;
            }*/
            int index=(int) Math.pow(2,i);
            ar_baru[index-1]=9;
        }
        
        for (int i=0;i<ar_baru.length;i++){ // memberi nilai 2 pada nilai yang akan diisi (sebagai penanda)
            if (ar_baru[i]==0){
                ar_baru[i]=2;
            }
        }
        for (int i=0;i<ar_baru.length;i++){ // proses pengisian
            if (ar_baru[i]==2){
                ar_baru[i]=ar_lama[j];
                j++;
                if (j==ar_baru.length){
                    j=ar_baru.length-1;
                }
                /*if (j==8){
                    j=8;
                }*/   
            }
        }
        return ar_baru;
    }
    
    public static int[] operasi(int[] arr,int pangkat){
        int[] ar_baru2=arr;
        int buat_tampung_pangkat = 0;
        String a="";
        for (int i=0;i<pangkat;i++){
            int index=(int) Math.pow(2,i);
            String aa= Integer.toString(index);
            if(i!=pangkat-1){
                a= a+aa+",";
            }
            else{
                a=a+aa;
            }
            buat_tampung_pangkat=i;
        }

        int hasil=0;
        int cek=0;
        int skip=0;
        int ii=0;        
        
        for (int i=0;i<ar_baru2.length;i++){//cek keseluruhan mulai dari 0
            if (ar_baru2[i]==9){// cari yang isinya 9
                cek=i+1;
                skip=cek+1;
                for (int j=i;j<ar_baru2.length;){ //cek keseluruhan tapi mulai dari index yang isinya 9
                    for(int jj=0;jj<cek;jj++){// melakukan penjumlahan berurutan sebanyak cek 
                        if(j==ar_baru2.length){// ini biar gak melebihi index out of bounds
                            break;
                        }
                        hasil=hasil+ar_baru2[j];
                        ii=j;//menampung nilai terakhir j ke ii agar nnti skipnya tidak mulai dari j tapi mulai dari ii
                        j++;
                    }
                    j=ii+skip;
                }
                //System.out.println(hasil-9);
                if (hasil%2==0){
                    ar_baru2[i]=1;
                }
                else{
                    ar_baru2[i]=0;
                }
                hasil=0;// reset hasil jadi 0 lagi 
            }      
        }
        System.out.println(Arrays.toString(ar_baru2));
        return ar_baru2;
    }
    
    public static int[] error_checking(int[] arr,int[] dua_pangkat,int ukuran){
        int[] ar_baru2=arr;
        int[] ar_baru3=new int[ukuran];
        int[] integers=dua_pangkat;
        int hasil=0;
        int hasil2=0;
        int cek=0;
        int skip=0;
        int integers_pointers=0;
        String status="benar";   

        // proses error correcting modif
        int ii=0;
        for (int i=0;i<ar_baru2.length;i++){//cek keseluruhan mulai dari 0
            if (integers_pointers==integers.length){// agar tidak out of bounds
                break;
            }
            if (i==integers[integers_pointers]-1){// cari yang sesuai pangkat-1
                System.out.println("");
                System.out.print("pangkat ="+(i+1));
                cek=i+1;
                skip=cek+1;
                for (int j=i;j<ar_baru2.length;){ //cek keseluruhan tapi mulai dari index yang isinya 9
                    for(int jj=0;jj<cek;jj++){// melakukan penjumlahan berurutan sebanyak cek 
                        if(j==ar_baru2.length){// ini biar gak melebihi index out of bounds
                            break;
                        }
                        hasil=hasil+ar_baru2[j];
                        ii=j;//menampung nilai terakhir j ke ii agar nnti skipnya tidak mulai dari j tapi mulai dari ii
                        j++;
                    }
                    j=ii+skip;
                }
                System.out.print(" Hasil ="+hasil);
                //System.out.println(hasil-9);
                if (hasil%2!=0){
                    hasil2=hasil2+i+1;
                    status="salah";
                }
                integers_pointers++;
                hasil=0;// reset hasil jadi 0 lagi 
            }      
        }        
        System.out.println("");
        if (status=="salah"){
            System.out.println("index array yang salah adalah "+(hasil2-1));
            if(ar_baru2[hasil2-1]==0){
                ar_baru2[hasil2-1]=1;
            }
            else{
                ar_baru2[hasil2-1]=0;
            }
        }
        System.out.println(""); 
        
        System.out.println("Proses Pemindahan ......");
        // Proses Deocoding
        int x=0;
        int y=0;
        for (int i=0;i<ar_baru2.length;i++){
            if (i==(integers[x]-1)){// seleksi nilai mana yang harus masukkan ke array yang baru
                x++;
                if (x==integers.length){ // mencegah agar tidak out of bounds (agar kalo x sudah sampai ujung tetap diujung array saja)
                    x=x-1;
                }
            }
            else{
                if(y==ar_baru3.length){// mencegah agar tidak out of bounds (kalau sudah sampai ujung array baru maka sudah selesai prosesnya 
                    break;
                }
                System.out.println("array baru index = "+y+" di isi dengan array lama index= "+i+", isi array lama ="+ar_baru2[i]);
                ar_baru3[y]=ar_baru2[i];
                y++;                
            }
        }                  
        return ar_baru3;
    }
    
    public static String encode(String str) {
        String[] array_string = str.split(","); 
        int ukuran=array_string.length;
        int[] input=new int[ukuran];
  
        for(int i=0; i<ukuran; i++) {
            input[i] = Integer.parseInt(array_string[i]);
            System.out.print(input[i]);
        }
        
        // Proses mencari r
        int r = 1;
        int M=ukuran;
        while (Math.pow(2, r) < (M + r + 1)) {  // Mencari nilai r yang terbesar
            r++; 
        }
        
        int ukuran_baru=M+r;
        int[] output=new int[ukuran_baru];
        output=letakan_redundan(input, ukuran, ukuran_baru, r);
        output=operasi(output, r);
        

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < output.length; i++) {
            stringBuilder.append(output[i]);
        }
        return stringBuilder.toString();
        }

    
    public static int[] decode(String code) {
    	int r=0;
    	String[] array_string = code.split(",");
    	
    	 int ukuran=array_string.length;
         int[] input=new int[ukuran];
         for(int i=0; i<ukuran; i++) {
             input[i] = Integer.parseInt(array_string[i]);
    	code=" "+code;
    	char[] arr=code.toCharArray();
    	do {
    		r+=1;
    	} while((int)Math.pow(2, r)<code.length());
    	int m = code.length()-1-r;
    	
         }
         int[] array_pangkat=new int[r];
         for (int i=0;i<r;i++){// proses memasukan nilai 2 pangkat
             array_pangkat[i]=(int) Math.pow(2,i);
         }
     	return error_checking(input, array_pangkat, ukuran);
    }
    
    
    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                int[] data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + Arrays.toString(data));
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}
